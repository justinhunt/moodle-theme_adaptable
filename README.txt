HTML code for settings and block areas

Front page info block:

The html for this block should look like:
<div class="span9">
	<h4>Information</h4>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
	</div>
	
	<div class="span3">
	<a href="#" class="submit">2013/14 Courses <i class="fa-chevron-right fa"></i></a>
</div>


Frontpage Marketing Block HTML structure BCU

 	<div><img src="/someimageURL.jpg" class="marketimage"></div>
    <div class="internalmarket">
    <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</h3>
    <a href="#" class="submit">My Home</a>
 	</div>
    
Frontpage slider markup:

<div class="span6 col-sm-6">
<h3>Hand-crafted</h3> <h4>pixels and code for the Moodle community</h4>
<a href="#" class="submit">Check out our custom theme pricing!</a>
</div>
 	
Frontpage Marketing Block HTML structure Coventry


<div><img src="http://somewebsite.com/2.jpg" class="marketimage"></div>
<h4><a href="#">International Courses</a></h4>
<p>Some text below the link....</p>

 	
Frontpage Secondary info Message HTML:
 
 <div class="span2 personpic">
		<div id="person" class="spn5">
		<img src="/urltoanimage.jpg" alt="person">
		</div>
		</div>
		
		<div class="span10">
		<h4>School of Jewelry</h4>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
		<a href="#" class="submit">Learn more... <i class="fa-chevron-right fa"></i></a>
		</div>
		
Footer HTML:

Sample Address Block for Footer:

 <ul class="block-list mbm">
     <li class="fn org">Moodle University</li>
     <li class="street-address">Moodle House</li>
     <li class="street-address">15 LMS Row</li>
     <li class="locality">Learnville</li>
     <li class="postal-code">Zip Code</li>
     <li class="country-name">United Kingdom</li>
     <li class="pts tel">+44 (0)123 456 789</li>
</ul>

Sample List Block with Chevron for Footer:

 <ul class="block-list white">
     <li><a href="http://moodle.org/"><span class="icon-right-open-mini"></span><span>Accessibility</span></a></li>
     <li><a href="http://moodle.org/"><span class="icon-right-open-mini"></span><span>Moodle Help</span></a></li>
     <li><a href="http://moodle.org/"><span class="icon-right-open-mini"></span><span>Moodle Feedback</span></a></li>
     <li><a href="http://moodle.org/"><span class="icon-right-open-mini"></span><span>IT Help</span></a></li>
     <li><a href="http://moodle.org/"><span class="icon-right-open-mini"></span><span>IT Feedback</span></a></li>
 </ul>
 
 +Sample code for Tools menu using FA Icons:

<span class="fa fa-video-camera"></span>Record Screen|http://google.co.uk|Record Screen
<span class="fa fa-picture-o"></span>ThinkStock|http://google.co.uk|ThinkStock
<span class="fa fa-clock-o"></span>Exam Clock|http://google.co.uk|Exam Clock
<span class="fa fa-share-alt"></span>Share a File|http://google.co.uk|Share a File
<span class="fa fa-globe"></span>Explor|http://google.co.uk|Explor
<span class="icon-my-cat"></span>MyCat|http://google.co.uk|MyCat